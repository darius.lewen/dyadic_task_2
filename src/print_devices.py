import pyglet
from evdev import InputDevice

mice = pyglet.input.evdev.get_devices()
screens = pyglet.canvas.get_display().get_screens()

dev = InputDevice('/dev/input/by-id/usb-Logitech_G502_HERO_Gaming_Mouse_045E38593231-event-mouse')

for event in dev.read_loop():
    print(event)

