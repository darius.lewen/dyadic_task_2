from pyglet.shapes import Rectangle, Circle
from pyglet.sprite import Sprite
import misc.create_images as images
import misc.conf as conf


class PredictionDistribution:
    def __init__(self, *args, **kwargs):
        self._bars = list()
        self._target_sprites = list()
        width = 15
        offset = 15
        for i in range(3):
            self._bars.append(Rectangle(x=1400 + i * (offset + width), y=900, width=width, height=200,
                                        color=(255, 255, 255), *args, **kwargs))
        fraction = conf.coop_score_high / conf.max_target_score
        self._target_sprites.append(images.get_target_image(is_competitive=True))
        self._target_sprites.append(images.get_target_image(is_competitive=False, high_p_idx=0, fraction=fraction))
        self._target_sprites.append(images.get_target_image(is_competitive=False, high_p_idx=1, fraction=fraction))
        self._target_sprites = [Sprite(t, *args, **kwargs) for t in self._target_sprites]
        for i, target_sprite in enumerate(self._target_sprites):
            target_sprite.update(x=1400 + width / 2 + i * (offset + width), y=900 - 10, scale=0.07)

    def update(self, predictions):
        for probability, bar in zip(predictions, self._bars):
            bar.height = 80 * probability

