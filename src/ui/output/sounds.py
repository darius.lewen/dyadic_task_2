from pyglet.media import load, Player
from os.path import realpath, dirname
import pyglet
# pyglet.options['audio'] = ('pulse',)
pyglet.options['audio'] = ('openal',)
# pyglet.options['search_local_libs'] = True
# pyglet.options["debug_media"] = True

dir_path = dirname(realpath(__file__)) + '/../../../res/sound/'
occupying_sound = load(dir_path + 'occupying.wav', streaming=False)
scored_sound = load(dir_path + 'scored.wav', streaming=False)
slipped_off_sound = load(dir_path + 'slipped_off.wav', streaming=False)


class OccupationSoundPlayer(Player):
    def __init__(self, volume=1.):
        super().__init__()
        self._reward_player = Player()
        self._reward_player.volume = volume
        self.volume = volume

    def occupying(self):
        self._force_play(occupying_sound)

    def slipped_off(self):
        self._force_play(slipped_off_sound)

    def scored(self):
        self._reward_player.queue(scored_sound)
        self._reward_player.play()

    def _force_play(self, sound):
        self.delete()
        self.next_source()
        self.queue(sound)
        self.play()

