from abc import ABC, abstractmethod
import misc.conf as conf
import misc.color as color
import misc.create_images as images
from ui.output.sounds import OccupationSoundPlayer
from pyglet.sprite import Sprite
from pyglet.shapes import Circle
import pyftdi

occ_sound_player = OccupationSoundPlayer()

class Target(Sprite, ABC):
    def __init__(self, *args, **kwargs):
        image = self._get_image()
        groups = kwargs['groups']
        del kwargs['groups']
        super().__init__(image, group=groups[0], *args, **kwargs)
        self._radius = (conf.field_size - 2 * conf.target_radius * conf.field_size) * conf.target_radius
        self._occupation_circle = Circle(0, 0, self._radius, group=groups[1],
                                         color=color.background, *args, **kwargs)
        self.update(0, 0, 0, self._radius * 2 / image.height)
        self.position = self._e_target.position
        self._is_conquered = False

    def tick(self):
        self._occupation_circle.radius = self._radius * self._e_target.occupation_progress
        if self._is_conquered != self._e_target._is_conquered():
            if self._is_conquered:
                occ_sound_player.slipped_off()
            else:
                occ_sound_player.occupying()
            self._is_conquered = not self._is_conquered

    @abstractmethod
    def _get_image(self):
        pass

    @property
    def position(self):
        return self._x, self._y

    @position.setter
    def position(self, position):
        if self._occupation_circle.position != tuple(position):
            self._on_scored()
        self._occupation_circle.position = position
        self._x, self._y = position
        self._update_position()

    def _on_scored(self):
        occ_sound_player.scored()
        self._is_conquered = False

class CompTarget(Target):
    def __init__(self, e_target, *args, **kwargs):
        self._e_target = e_target
        self._occ_agent = e_target.occupying_agent
        super().__init__(*args, **kwargs)

    def _get_image(self):
        return images.get_target_image(fraction=self._e_target.fraction(),
                                       is_competitive=True,
                                       comp_p_idx=self._occ_agent)

    def tick(self):
        if self._occ_agent != self._e_target.occupying_agent:
            if self._occ_agent is not None and self._e_target.occupying_agent is not None:
                occ_sound_player.slipped_off()
                occ_sound_player.occupying()
            self._occ_agent = self._e_target.occupying_agent
            self.image = self._get_image()
        super().tick()

class CoopTarget(Target):
    def __init__(self, e_target, *args, **kwargs):
        self._e_target = e_target
        self._high_p_idx = self._e_target.high_player_idx
        super().__init__(*args, **kwargs)

    def _get_image(self):
        return images.get_target_image(fraction=self._e_target.fraction(),
                                       is_competitive=False,
                                       high_p_idx=self._high_p_idx)


class PunishingTarget(CompTarget):
    def __init__(self, e_target, *args, **kwargs):
        self._e_target = e_target
        super().__init__(e_target, *args, **kwargs)

    def _get_image(self):
        return images.get_target_image(fraction=self._e_target.fraction(),
                                       is_punishing=True,
                                       comp_p_idx=self._occ_agent)

