from pyglet.sprite import Sprite
import misc.conf as conf
from misc.create_images import get_aim_image


class Aim(Sprite):
    def __init__(self, player_idx, *args, **kwargs):
        super().__init__(img=get_aim_image(player_idx), *args, **kwargs)
        self._radius = (conf.field_size - 2 * conf.target_radius * conf.field_size) * conf.aim_radius
        self.update(0, 0, 0, self._radius * 2 / self.image.height)

