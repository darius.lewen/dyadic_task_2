from pyglet.shapes import Circle
import misc.conf as conf
import misc.color as color


class Agent(Circle):
    def __init__(self, p_color, groups, *args, **kwargs):
        super().__init__(-1000, 0, conf.agent_radius * conf.field_size,
                         color=color.black,
                         group=groups[0],
                         *args, **kwargs)
        self._inner = Circle(-1000, 0, conf.agent_radius * conf.field_size - 2,
                             color=p_color,
                             group=groups[1],
                             *args, **kwargs)

    @property
    def position(self):
        return super().position

    @position.setter
    def position(self, position):
        super(Circle, self.__class__).position.__set__(self, position)
        self._inner.position = position


