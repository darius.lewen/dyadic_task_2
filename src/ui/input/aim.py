from abc import ABC, abstractmethod
import pyglet
import numpy
import misc.conf as conf
from pyglet import clock

evdev_devices = pyglet.input.evdev.get_devices()

mice = [device for device in evdev_devices if device.name in conf.mice]
mice = sorted(mice, key=lambda x: conf.mice.index(x.name), reverse=True)

touch_panels = list()
for device in evdev_devices:
    if 'PQLabs' in device.name and len(device.control_map) == 10:
        touch_panels.append(device)


def get_mouse():
    mouse = mice.pop()
    mouse.open()
    pyglet.app.platform_event_loop._select_devices.remove(mouse)
    clock.schedule(lambda _: mouse.select())
    return mouse


def get_panel():
    panel = touch_panels.pop()
    panel.open()
    return panel


class Aim(ABC):
    def __init__(self, engine):
        self._engine = engine
        self._position = numpy.array([.5, .5])
        self._register_handlers()

    @abstractmethod
    def _register_handlers(self):
        pass

    def _bound(self):
        for dimension in [0, 1]:
            if self._position[dimension] > 1.:
                self._position[dimension] = 1.
            elif self._position[dimension] < 0.:
                self._position[dimension] = 0.

    @property
    def position(self):
        return self._position


class MouseAim(Aim):

    def _register_handlers(self):
        mouse = get_mouse()
        for control in mouse.get_controls():
            if isinstance(control, pyglet.input.base.RelativeAxis):
                self._register_handler(control, invert_x=len(mice) == 0)

    def _register_handler(self, control, invert_x):
        @control.event
        def on_change(value):
            if self._engine.running:
                if control.name == 'x':
                    if invert_x and conf.invert_second_mouse:  # invert 2nd mouse
                        value *= -1
                    self._position[0] += value * conf.mouse_sensitivity
                elif control.name == 'y':
                    self._position[1] -= value * conf.mouse_sensitivity
                self._bound()
        #control.event(on_change)


class TouchAim(Aim):

    def _register_handlers(self):
        panel = get_panel()
        for control in panel.get_controls():
            if isinstance(control, pyglet.input.base.AbsoluteAxis):
                self._register_handler(control, invert_x=len(touch_panels) == 0)

    def _register_handler(self, control, invert_x):
        @control.event
        def on_change(value):
            if self._engine.running:
                if control.name == 'x':
                    #value = 1 - value / control.max if invert_x else value / control.max # todo enable this when screen is right again...
                    value = value / control.max
                    #self._position[0] = value
                    self._position[0] = (value - conf.rel_field_x_offset) / conf.rel_field_width# todo this scaling was for panel fits screen setup
                elif control.name == 'y':
                    value = value / control.max if invert_x else 1 - value / control.max  # todo this is a hotfix for the bottom up screen....
                    #self._position[1] = value
                    self._position[1] = (value - conf.rel_field_y_offset) / conf.rel_field_height
                    #self._position[1] = ((1 - value / control.max) - conf.rel_field_y_offset) / conf.rel_field_height
                    #self._position[1] = (value - conf.rel_field_y_offset) / conf.rel_field_height  # todo this scaling was for panel fits screen setup
                self._bound()
