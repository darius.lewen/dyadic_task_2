import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score

import misc.conf as conf
import misc.paths as paths

plot_counter = 0


def prepare_data(gaze_data, target_pos):

    if isinstance(gaze_data, list):
        gaze_data = pd.DataFrame(list(gaze_data), columns=['x_gaze', 'y_gaze', 'gaze_timestamp'])
    if isinstance(target_pos, list):
        target_pos = pd.DataFrame(list(target_pos), columns=['x_target', 'y_target', 'target_timestamp'])

    pos_data = target_pos[::]
    __temp = np.zeros((pos_data.shape[0], 2))
    __temp[::] = np.nan
    pos_data = pd.concat([pos_data, pd.DataFrame(__temp, columns=['x_gaze', 'y_gaze'])],
                         axis=1)
    for ii in range(len(pos_data['target_timestamp'])-1):
        t = pos_data['target_timestamp'].iloc[ii:ii+2]
        __temp = gaze_data.loc[(gaze_data['gaze_timestamp'] >= t.iloc[0]) & (gaze_data['gaze_timestamp'] < t.iloc[1])]
        if __temp.shape[0] != 0:
            pos_data['x_gaze'].iloc[ii] = __temp['x_gaze'].mean()
            pos_data['y_gaze'].iloc[ii] = __temp['y_gaze'].mean()

    pos_data = pos_data[pos_data['x_gaze'].notna() & pos_data['y_gaze'].notna()]
    return pos_data


def poly_fit(pos_data, polynom_degree=1):
    # print(len(pos_data['x_target']), len(pos_data['x_gaze']))
    models = dict.fromkeys(['x-axis', 'y-axis'])
    models['x-axis'] = np.polyfit(pos_data['x_target'], pos_data['x_gaze'], polynom_degree)  # type error if not synced, x_target will be empty
    models['y-axis'] = np.polyfit(pos_data['y_target'], pos_data['y_gaze'], polynom_degree)
    return models


def confidence_threshold(gaze_data):  # on idx 3 is the confidence saved
    return [gaze[:3] for gaze in gaze_data if gaze[3] > conf.gaze_confidence_threshold]


def get_gaze_scaling_func(gaze_data, target_pos):
    gaze_data = confidence_threshold(gaze_data)
    pos_data = prepare_data(gaze_data, target_pos)
    model = poly_fit(pos_data)

    def scale_gaze(x, y):
        x = (x - model['x-axis'][1]) / model['x-axis'][0]
        y = (y - model['y-axis'][1]) / model['y-axis'][0]
        return x, y

    # plot_calibration(pos_data, model)  # todo reimplement?
    return scale_gaze, model


# A function to plot calibration results
def plot_calibration(pos_data, models, title='Title'):
    fig = plt.figure(figsize=(20, 10))
    fig.suptitle(title)
    ax = []
    for ii in range(1, 5):
        ax.append(plt.subplot(2, 2, ii))
    x_fn = np.linspace(0, 1, 20)
    ax[0].plot(pos_data['target_timestamp'], pos_data['x_target'], 'black', linewidth=4)
    ax[0].plot(pos_data['target_timestamp'], pos_data['x_gaze'], 'blue', linewidth=1.5)
    ax[0].plot(pos_data['target_timestamp'], (pos_data['x_gaze'] - models['x-axis'][1]) / models['x-axis'][0], 'red',
               linewidth=2)
    ax[0].plot(pos_data['target_timestamp'], np.linspace(1, 1, len(pos_data['target_timestamp'])), '--', color='black', alpha=0.5)
    ax[0].plot(pos_data['target_timestamp'], np.linspace(0, 0, len(pos_data['target_timestamp'])), '--', color='black', alpha=0.5)
    ax[0].set_title('x-Axis')
    r2 = r2_score(pos_data['x_target'], (pos_data['x_gaze'] - models['x-axis'][1]) / models['x-axis'][0])
    print(f'r²_x = {r2}')

    ax[2].plot(pos_data['target_timestamp'], pos_data['y_target'], 'black', linewidth=4)
    ax[2].plot(pos_data['target_timestamp'], pos_data['y_gaze'], 'blue', linewidth=1.5)
    ax[2].plot(pos_data['target_timestamp'], (pos_data['y_gaze'] - models['y-axis'][1]) / models['y-axis'][0], 'red',
               linewidth=2)
    ax[2].set_title('y-Axis')
    ax[2].plot(pos_data['target_timestamp'], np.linspace(1, 1, len(pos_data['target_timestamp'])), '--', color='black', alpha=0.5)
    ax[2].plot(pos_data['target_timestamp'], np.linspace(0, 0, len(pos_data['target_timestamp'])), '--', color='black', alpha=0.5)
    r2 = r2_score(pos_data['y_target'], (pos_data['y_gaze'] - models['y-axis'][1]) / models['y-axis'][0])
    print(f'r²_y = {r2}')

    ax[1].plot(pos_data['x_target'], pos_data['x_gaze'], '+', color='blue', markersize=3)
    ax[1].plot(x_fn, np.poly1d(models['x-axis'])(x_fn), "red", linewidth=3)
    ax[1].set_xlabel('x_target')
    ax[1].set_ylabel('x_gaze')
    ax[1].set_aspect('equal')
    ax[1].set_xlim([0, 1])
    ax[1].set_ylim([0, 1])
    ax[1].set_title('x-Axis fit')

    ax[3].plot(pos_data['y_target'], pos_data['y_gaze'], '+', color='blue', markersize=3)
    ax[3].plot(x_fn, np.poly1d(models['y-axis'])(x_fn), "red", linewidth=3)
    ax[3].set_xlabel('y_target')
    ax[3].set_ylabel('y_gaze')
    ax[3].set_aspect('equal')
    ax[3].set_xlim([0, 1])
    ax[3].set_ylim([0, 1])
    ax[3].set_title('y-Axis fit')
    global plot_counter
    plt.savefig(paths.gaze_fits[plot_counter])
    plot_counter += 1
    #plt.show()

