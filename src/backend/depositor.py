import time
import json
import pkg_resources
# pkg_resources.require('tables==3.6.1')
import tables
import os
import stat
import misc.conf as conf
import subprocess
from misc.paths import recordings_folder
import misc.paths as paths


class Depositor:
    def __init__(self, gaze_collectors=None):
        self._gaze_collectors = gaze_collectors
        self._record_file = None
        self._models = None
        self._paths = dict()
        self._init_paths()
        self._deposit_conf()

    def _init_paths(self):
        folder = _get_folder()
        paths.gaze_fits[0] = f'{folder}/pre_fit_p0.png'
        paths.gaze_fits[1] = f'{folder}/pre_fit_p1.png'
        paths.gaze_fits[2] = f'{folder}/post_fit_p0.png'
        paths.gaze_fits[3] = f'{folder}/post_fit_p1.png'
        self._paths['record'] = folder + '/record.h5'
        self._paths['conf'] = folder + '/conf.json'
        self._paths['misc'] = folder + '/misc.json'
        self._paths['pre_gaze'] = folder + '/pre_gaze.json'
        self._paths['main_gaze'] = folder + '/main_gaze.json'
        self._paths['post_gaze'] = folder + '/post_gaze.json'
        for path in self._paths.values():
            os.mknod(path, mode=0o777 | stat.S_IRUSR)

    @property
    def gaze_collectors(self):
        return self._gaze_collectors

    @property
    def models(self):
        return self._models

    @models.setter
    def models(self, models):
        self._models = models

    def init_record_file(self, game_shape):
        self._record_file = tables.open_file(self._paths['record'], mode='w')
        self._record_file.create_earray(where=self._record_file.root,
                                        name='data',
                                        atom=tables.Float64Atom(),
                                        shape=(0, *game_shape))

    def close(self):
        if self._gaze_collectors is not None:
            for collector in self._gaze_collectors:
                collector.stop()
        self._record_file.close()

    def deposit(self, game_state):
        self._record_file.root.data.append(game_state[None, :])

    def get_length(self):
        return len(self._record_file.root.data)

    def deposit_gaze(self, prefix, start_time, end_time):
        if self._gaze_collectors is None:
            return
        _gaze_data = {
            f'p0_gaze': self._gaze_collectors[0].drain_gaze_data(start_time, end_time),
            f'p1_gaze': self._gaze_collectors[1].drain_gaze_data(start_time, end_time),
        }
        with open(self._paths[f'{prefix}_gaze'], 'w') as file:
            json.dump(_gaze_data, file)

    def load_gaze(self, prefix):
        with open(self._paths[f'{prefix}_gaze']) as file:
            json_file = json.load(file)
            gaze_p0 = json_file['p0_gaze']
            gaze_p1 = json_file['p1_gaze']
            return gaze_p0, gaze_p1

    def deposit_misc(self, start_date):
        try:
            game_version = subprocess.check_output(['git', 'describe', '--tags'])
        except subprocess.CalledProcessError as e:
            print('Failed to call \'git describe --tags\': Please create a Git tag so that each recording can be traced back to a code base.')
            raise e
        _misc = {
            'start_date': start_date,
            'end_date': time.time(),
            'p0_id': conf.p0_id,
            'p1_id': conf.p1_id,
            'game_version': str(game_version),
        }
        if self._models is not None:
            _misc = {
                **_misc,
                'p0_poly_x': list(self._models[0]['x-axis']),
                'p0_poly_y': list(self._models[0]['y-axis']),
                'p1_poly_x': list(self._models[1]['x-axis']),
                'p1_poly_y': list(self._models[1]['y-axis']),
            }
        with open(self._paths['misc'], 'w') as file:
            json.dump(_misc, file)

    def _deposit_conf(self):
        _configuration = {item: getattr(conf, item) for item in dir(conf)
                          if not item.startswith("__") and not item.endswith("__")}
        with open(self._paths['conf'], 'w') as file:
            json.dump(_configuration, file)


def _get_folder():
    folder = f'{recordings_folder}/{str(conf.p0_id).zfill(3)}_{str(conf.p1_id).zfill(3)}'
    if not os.path.exists(folder):
        os.makedirs(folder)
    recording_num = 0
    while os.path.exists(folder + f'/{recording_num}'):
        recording_num += 1
    folder = folder + f'/{recording_num}'
    os.makedirs(folder)
    return folder
