import numpy
import misc.conf as conf


class Agent:
    def __init__(self, actor_aim, speed=conf.max_speed):
        self._position = numpy.array([.5, .5])
        self._actor_aim = actor_aim
        self._speed = speed

    def tick(self):
        vec_to_aim = self._actor_aim.position - self._position
        dist_to_aim = numpy.linalg.norm(vec_to_aim)
        if dist_to_aim > conf.max_speed:
            self._position += vec_to_aim / dist_to_aim * self._speed
        else:
            self._position += vec_to_aim

    @property
    def position(self):
        return self._position
