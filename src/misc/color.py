background = (0, 0, 0)
field_border = (255, 255, 255)
black = (32, 32, 32)
players = [
    (87, 117, 180),
    (250, 114, 44)
]
gaze = [
    (47, 77, 140),
    (210, 74, 4)
]
competitive_target = [
    (32, 32, 32),
    (255, 255, 255)
]
punishing_target = [
    (32, 32, 32),
    (210, 100, 100),
    (255, 255, 255),
]
calibration_agent = (255, 255, 255)
score = (255, 255, 255)
scores = [(250, 114, 44), (87, 117, 180)]

