import numpy as np
import pyglet
import misc.conf as conf


def load_img(path):
    img = pyglet.image.load(path)
    img.anchor_x = img.width // 2
    img.anchor_y = img.height // 2
    return img


def to_win_pos(engine_pos):
    x, y = engine_pos if isinstance(engine_pos, (tuple, np.ndarray)) else engine_pos.position
    x *= conf.field_size - 2 * conf.target_radius * conf.field_size
    y *= conf.field_size - 2 * conf.target_radius * conf.field_size
    x += conf.field_x_offset + conf.target_radius * conf.field_size
    y += conf.field_y_offset + conf.target_radius * conf.field_size
    return x, y


def print_devices():
    print('\nSelect mice form here:')
    for device in pyglet.input.evdev.get_devices():
        print(f'\'{device.name}\'')
    print('\nSelect screen indices form these:')
    for i, screen in enumerate(pyglet.canvas.get_display().get_screens()):
        print(f'index={i} x={screen.x} y={screen.y}')



