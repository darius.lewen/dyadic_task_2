import misc.color as color
import misc.conf as conf
import pyglet
from PIL import Image, ImageDraw, ImageFont, ImageOps
import numpy as np


PI = 3.141593

aim_indicator_size = 256
rectangle_height = aim_indicator_size // 4
rectangle_width = aim_indicator_size // 12
rectangle_coordinates = [
    (
        (aim_indicator_size - rectangle_width) // 2,  # upper left x
        0,                                            # upper left y
        (aim_indicator_size + rectangle_width) // 2,  # lower right x
        rectangle_height                              # lower right y
    ),
    (
        (aim_indicator_size - rectangle_width) // 2,  # upper left x
        aim_indicator_size - rectangle_height,        # upper left y
        (aim_indicator_size + rectangle_width) // 2,  # lower right x
        aim_indicator_size                            # lower right y
    ),
    (
        0,                                            # upper left x
        (aim_indicator_size - rectangle_width) // 2,  # upper left y
        rectangle_height,                             # lower right x
        (aim_indicator_size + rectangle_width) // 2   # lower right y
    ),
    (
        aim_indicator_size - rectangle_height,        # upper left x
        (aim_indicator_size - rectangle_width) // 2,  # upper left y
        aim_indicator_size,                           # lower right x
        (aim_indicator_size + rectangle_width) // 2   # lower right y
    ),
]


def _radian_to_degree(angle):
    return angle*180/PI


def _fraction_to_degree(fraction):
    rotation_in_radian = fraction * 2 * PI
    pillow_offset = PI * 3/2
    return _radian_to_degree(rotation_in_radian + pillow_offset)


def _pil_to_pyglet(pil_image):
    image = pyglet.image.ImageData(pil_image.width,
                                   pil_image.height,
                                   format='RGBA',
                                   data=pil_image.tobytes(),
                                   pitch=pil_image.width * -4)
    return image


def _center_image(image):
    image.anchor_x = image.width // 2
    image.anchor_y = image.height // 2
    return image


def get_replay_info_image(text_lines, mode='RGBA'):
    out = Image.new(mode, (conf.replay_info_width, conf.replay_info_height * (1 + len(text_lines))))
    y_pos = 0
    for text_line in text_lines:
        image = Image.new(mode, (conf.replay_info_width, conf.replay_info_height), (0, 0, 0, 0))
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype(conf.font, conf.replay_info_height - 10)
        draw.text((0, 0), text_line, fill=color.score, font=font)
        out.paste(image, (0, y_pos))
        y_pos += image.height
    return _pil_to_pyglet(out)


def get_score_image(score, mirror=False, flip=False, both_rewards=False, player=None):
    if both_rewards:
        image = Image.new('RGBA', (conf.score_width, conf.score_height * 2), (0, 0, 0, 0))
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype(conf.font, int(conf.score_height * 0.8))
        if not flip and not mirror:
            draw.text((0, 0), f'{score[0] * conf.score_to_euro_factor:4.2f}€',
                      anchor="la", align='left', fill=color.scores[1], font=font)
            draw.text((0, 100), f'{score[1] * conf.score_to_euro_factor:4.2f}€',
                      anchor="la",  align='right', fill=color.scores[0], font=font)
        else:
            draw.text((0, 100), f'{score[0] * conf.score_to_euro_factor:4.2f}€',
                      anchor="la", align='left', fill=color.scores[1], font=font)
            draw.text((0, 0), f'{score[1] * conf.score_to_euro_factor:4.2f}€',
                      anchor="la", align='right', fill=color.scores[0], font=font)
    else:
        image = Image.new('RGBA', (conf.score_width, conf.score_height), (0, 0, 0, 0))
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype(conf.font, conf.score_height)
        draw.text((100, 0), f'{np.around(score * conf.score_to_euro_factor, decimals=2)}', anchor="ma", align='center',
                  fill=color.scores[player], font=font)
    if mirror:
        image = ImageOps.mirror(image)
    if flip:
        image = ImageOps.flip(image)
    return _pil_to_pyglet(image)


def get_target_image(fraction=1, is_punishing=False ,is_competitive=False, high_p_idx=None, comp_p_idx=None, image_size=256):
    if is_competitive:
        color_0 = color.competitive_target[0]
        if comp_p_idx is not None:
            color_1 = color.players[comp_p_idx]
        else:
            color_1 = color.competitive_target[1]
    elif is_punishing:
        color_0 = color.punishing_target[0]
        color_1 = color.punishing_target[1]
    else:
        color_0 = color.players[(1 + high_p_idx) % 2]
        color_1 = color.players[high_p_idx]

    image = Image.new('RGBA', (image_size, image_size), (0, 0, 0, 0))
    draw = ImageDraw.Draw(image)

    draw.ellipse((0, 0, image_size, image_size),
                 fill=color_0),
    draw.pieslice((0, 0, image_size, image_size),
                  start=_fraction_to_degree(0 + (1-fraction) / 2),
                  end=_fraction_to_degree(fraction + (1-fraction) / 2),
                  fill=color_1)

    if is_punishing:
        font = ImageFont.truetype(conf.font, 7 * int(conf.target_radius * conf.field_size))
        draw.text((image_size // 2, image_size // 2), '!', fill=color.punishing_target[2], font=font, anchor='mm')

    return _center_image(_pil_to_pyglet(image))


def get_aim_image(player_idx):
    image = Image.new('RGBA', (aim_indicator_size + 1, aim_indicator_size + 1), (0, 0, 0, 0))
    draw = ImageDraw.Draw(image)
    global rectangle_coordinates
    for rectangle in rectangle_coordinates:
        draw.rectangle(rectangle,
                       fill=color.players[player_idx],
                       outline=color.black, width=rectangle_width//8)
    image = image.rotate(45)
    return _center_image(_pil_to_pyglet(image))
